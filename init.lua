-- SPDX-License-Identifier: GPL-3.0-or-later
-- © 2020 Georgi Kirilov

require("vis")
local vis = vis

local start = {}
local finish = {}
local lastcol = {}

local nothing_inserted = true
local last_mode

local function finishof(selection)
	return finish[selection.number] and vis.win.file:mark_get(finish[selection.number])
end

local function startof(selection)
	local mark
	if start[selection.number] then
		if type(start[selection.number]) ~= "number" then
			mark = vis.win.file:mark_get(start[selection.number])
			mark = mark and mark + 1
		else
			mark = start[selection.number]
		end
	elseif finish[selection.number] then
		mark = vis.win.file:mark_get(finish[selection.number])
	end
	return mark
end

local function fence_setup(win)
	if win ~= vis.win then return end
	if last_mode == vis.mode then return end
	if vis.mode == vis.modes.INSERT or vis.mode == vis.modes.REPLACE then
		start = {}
		finish = {}
		lastcol = {}
		for selection in win:selections_iterator() do
			finish[selection.number] = win.file:mark_set(selection.pos)
			lastcol[selection.number] = selection.col
		end
	elseif vis.mode == vis.modes.NORMAL and last_mode == vis.modes.INSERT then
		-- Clean unused autoindent if we open a new empty line and then press Escape
		for selection in win:selections_iterator() do
			local line = win.file.lines[selection.line]
			local indented = line:find("^[ \t]+$")
			if indented and nothing_inserted then
				local eol = startof(selection)  -- O - OPEN_LINE_ABOVE
				if selection.pos ~= eol then
					eol = finishof(selection)  -- o - OPEN_LINE_BELOW
				end
				win.file:delete(eol - #line, #line)
				selection.pos = eol - #line
			end
		end
		nothing_inserted = true
	end
	last_mode = vis.mode
end

local function set_start()
	for selection in vis.win:selections_iterator() do
		if not start[selection.number] then
			-- XXX: at BOF, a 0 is pushed instead of a real mark, because pos - 1 would be invalid otherwise.
			-- startof() must be aware of this exception.
			start[selection.number] = selection.pos > 0 and vis.win.file:mark_set(selection.pos - 1) or 0
		end
	end
end

local function reset_finish()
	for selection in vis.win:selections_iterator() do
		finish[selection.number] = vis.win.file:mark_set(selection.pos + 1)
	end
end

local function save_lastcol(ahead)
	for selection in vis.win:selections_iterator() do
		lastcol[selection.number] = selection.col + ahead
	end
end

local function get_lastcol(selection)
	return lastcol[selection.number] or (selection.col)
end

local function newlines_between(pos1, pos2)
	if not (pos1 and pos2) or not (pos2 > pos1) then return 1 end
	local lines = 0
	for _ in vis.win.file:content(pos1, pos2 - pos1):gmatch("\n") do
		lines = lines + 1
	end
	return lines
end

local function on_start_line()
	local start_pos = startof(vis.win.selection)
	local len = vis.win.selection.pos - start_pos
	return not vis.win.file:content(start_pos, len):find("\n", 1, true)
end

local function delete_left(motion_id)
	return function()
		local start_pos, last_pos = {}, {}
		for selection in vis.win:selections_iterator() do
			last_pos[selection.number] = selection.pos
			start_pos[selection.number] = startof(selection)
		end
		local unpairs = vis_pairs and vis_pairs.unpair[vis.win] and vis_pairs.unpair[vis.win]()
		vis:motion(motion_id)
		for i = #vis.win.selections, 1, -1 do
			local selection = vis.win.selections[i]
			if selection.pos < start_pos[i] then
				selection.pos = start_pos[i]
				unpairs = nil
			end
			local pos = selection.pos
			vis.win.file:delete(pos, last_pos[i] - pos + (unpairs and unpairs[selection.number] or 0))
			selection.pos = pos
		end
	end
end

local function left(motion_id)
	return function()
		local start_pos = {}
		for selection in vis.win:selections_iterator() do
			local sel_start, sel_finish = startof(selection), finishof(selection)
			if sel_start and sel_finish and sel_finish >= selection.pos and sel_start <= selection.pos then
				start_pos[selection.number] = sel_start
			end
		end
		vis:motion(motion_id)
		for selection in vis.win:selections_iterator() do
			if start_pos[selection.number] and selection.pos < start_pos[selection.number] then
				selection.pos = start_pos[selection.number]
			end
		end
	end
end

local function right(motion_id)
	return function()
		local finish_pos = {}
		for selection in vis.win:selections_iterator() do
			local sel_start, sel_finish = startof(selection), finishof(selection)
			if sel_start and sel_finish and sel_finish >= selection.pos and sel_start <= selection.pos then
				finish_pos[selection.number] = sel_finish
			end
		end
		vis:motion(motion_id)
		for selection in vis.win:selections_iterator() do
			if finish_pos[selection.number] and selection.pos > finish_pos[selection.number] then
				selection.pos = finish_pos[selection.number]
			end
		end
	end
end

local hkey_handlers = {

	["<Delete>"] = function()
		vis:motion(17)  -- +/VIS_MOVE_CHAR_NEXT vis.h
		local last_pos = {}
		for selection in vis.win:selections_iterator() do
			last_pos[selection.number] = selection.pos
		end
		vis:motion(16)  -- +/VIS_MOVE_CHAR_PREV vis.h
		local unpairs = vis_pairs and vis_pairs.unpair[vis.win] and vis_pairs.unpair[vis.win]()
		for i = #vis.win.selections, 1, -1 do
			local selection = vis.win.selections[i]
			if selection.pos < finishof(selection) then
				local pos = selection.pos - (unpairs and unpairs[selection.number] or 0)
				vis.win.file:delete(pos, last_pos[i] - pos)
				selection.pos = pos
			else
				selection.pos = finishof(selection)
			end
		end
	end,

	["<Backspace>"] = delete_left(16),  -- +/VIS_MOVE_CHAR_PREV vis.h
	["<C-w>"] = delete_left(26),        -- +/VIS_MOVE_WORD_START_PREV vis.h
	["<C-u>"] = delete_left(8),         -- +/VIS_MOVE_LINE_BEGIN vis.h
	["<Left>"] = left(16),              -- +/VIS_MOVE_CHAR_PREV vis.h
	["<S-Left>"] = left(28),            -- +/VIS_MOVE_LONGWORD_START_PREV vis.h
	["<Right>"] = right(17),            -- +/VIS_MOVE_CHAR_NEXT vis.h
	["<S-Right>"] = right(29),          -- +/VIS_MOVE_LONGWORD_START_NEXT vis.h

	["<Home>"] = function()
		local start_pos, last_pos = {}, {}
		for selection in vis.win:selections_iterator() do
			local sel_start = startof(selection)
			if sel_start then
				last_pos[selection.number] = selection.pos
				start_pos[selection.number] = sel_start
			end
		end
		vis:motion(8)   -- +/VIS_MOVE_LINE_BEGIN vis.h
		for selection in vis.win:selections_iterator() do
			if start_pos[selection.number] and selection.pos < start_pos[selection.number] or
				last_pos[selection.number] == selection.pos then -- If already at BOL, move to the input entry point
				selection.pos = start_pos[selection.number]
			end
		end
	end,

	["<End>"] = function()
		local finish_pos, last_pos = {}, {}
		for selection in vis.win:selections_iterator() do
			local sel_finish = finishof(selection)
			if sel_finish then
				last_pos[selection.number] = selection.pos
				finish_pos[selection.number] = sel_finish
			end
		end
		vis:motion(12)  -- +/VIS_MOVE_LINE_END vis.h
		for selection in vis.win:selections_iterator() do
			if finish_pos[selection.number] and selection.pos > finish_pos[selection.number] or
				last_pos[selection.number] == selection.pos then -- If already at EOL, move to the input end point
				selection.pos = finish_pos[selection.number]
			end
		end
	end,

	["<Enter>"] = function()
		-- XXX: This is here because vis has vis_keys_feed(vis, "<Enter>") calls hardcoded in `o` and `O`,
		-- and I don't get to detect insert mode and initialize the fences.
		fence_setup(vis.win)
		set_start()
		vis:feedkeys("<vis-insert-newline>")
	end,

	["<Tab>"] = function()
		nothing_inserted = false
		set_start()
		vis:feedkeys("<vis-insert-tab>")
	end,

	["<S-Tab>"] = function()
		set_start()
		vis:feedkeys("<vis-selections-align-indent-left>")
	end,

	["<C-t>"] = function()
		if on_start_line() then return end
		local indent = vis.expandtab and string.rep(" ", vis.tabwidth) or "\t"
		for selection in vis.win:selections_iterator() do
			local start_pos = startof(selection)
			local last_pos = selection.pos
			local input = vis.win.file:content(start_pos, last_pos - start_pos)
			local cur_line_pos = input:find("\n?([ \t]*)%f[%S][^\n]*$")
			vis.win.file:insert(start_pos + cur_line_pos, indent)
			selection.pos = last_pos + #indent
		end
	end,

	["<C-d>"] = function()
		if on_start_line() then return end
		local indent = vis.expandtab and string.rep(" ", vis.tabwidth) or "\t"
		for selection in vis.win:selections_iterator() do
			local start_pos = startof(selection)
			local last_pos = selection.pos
			local input = vis.win.file:content(start_pos, last_pos - start_pos)
			local old_indent = input:match("\n?([ \t]*)%f[%S][^\n]*$")
			local cur_line_pos = input:find("\n?([ \t]*)%f[%S][^\n]*$")
			if old_indent and old_indent:match(indent .. "$") then
				vis.win.file:delete(start_pos + cur_line_pos, #indent)
				selection.pos = last_pos - #indent
			end
		end
	end,

}

local vkey_handlers = {

	["<Up>"] = function()
		local start_pos = startof(vis.win.selection)
		-- TODO: do this check for each selection individually
		local lines_past_start = newlines_between(start_pos, vis.win.selection.pos)
		if lines_past_start <= 0 then return end
		vis:motion(1)   -- +/VIS_MOVE_LINE_UP vis.h
		vis.count = get_lastcol(vis.win.selection) - 1
		vis:motion(15)  -- +/VIS_MOVE_COLUMN vis.h
		local force_align = false
		for selection in vis.win:selections_iterator() do
			local sel_start = startof(selection)
			if sel_start and selection.pos < sel_start then
				force_align = true
				break
			end
		end
		if force_align then
			for selection in vis.win:selections_iterator() do
				selection.pos = startof(selection)
			end
		end
	end,

	["<Down>"] = function()
		local finish_pos = finishof(vis.win.selection)
		-- TODO: do this check for each selection individually
		local lines_to_finish = newlines_between(vis.win.selection.pos, finish_pos)
		if lines_to_finish <= 0 then return end
		vis:motion(0)   -- +/VIS_MOVE_LINE_DOWN vis.h
		vis.count = get_lastcol(vis.win.selection) - 1
		vis:motion(15)  -- +/VIS_MOVE_COLUMN vis.h
		if vis.win.selection.pos > finish_pos then
			for selection in vis.win:selections_iterator() do
				selection.pos = finishof(selection)
			end
		end
	end,

	["<PageUp>"] = function() end,
	["<PageDown>"] = function() end,
	["<S-PageUp>"] = function() end,
	["<S-PageDown>"] = function() end,

}

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
	for key, handler in pairs(vkey_handlers) do
		win:map(vis.modes.INSERT, key, handler)
	end
	for key, handler in pairs(hkey_handlers) do
		win:map(vis.modes.INSERT, key, function() handler() save_lastcol(0) end)
	end
end)

vis.events.subscribe(vis.events.WIN_STATUS, fence_setup)

local function on_input()
	nothing_inserted = false
	save_lastcol(1)
	set_start()
	if vis.mode == vis.modes.REPLACE then
		reset_finish()
	end
end

vis.events.subscribe(vis.events.INPUT, on_input, 1)
